<%--
  Created by IntelliJ IDEA.
  User: student01
  Date: 7/14/20
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add To My WishList</title>
</head>
<body>

<h1>Add an item to my WishList</h1>

<form action="/products/add-item" method="post">
    <input type="text" name="name" id="name" placeholder="Enter name">
    <br>
    <input type="text" name="category" id="category" placeholder="Enter category">
    <br>
    <input type="text" name="price" id="price" placeholder="Enter price">
    <br>
    <input type="submit">
</form>

</body>
</html>
