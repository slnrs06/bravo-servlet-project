package StudentApp;

import java.util.List;

// DAO Interface
public interface Students {
    List<Student> all(); // get all the students record/data

    void insert(Student student); // add a new student to our database
}
