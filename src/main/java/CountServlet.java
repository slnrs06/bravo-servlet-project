import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CountServlet", urlPatterns = "/count")
public class CountServlet extends HttpServlet {
    // create a variable
    private int counter = 0;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //increment the variable
//        counter++;
        counter += 1;

// set the content type
        response.setContentType("text/html");


// get the PrintWriter
        PrintWriter writer = response.getWriter();

        //generate the html content
        writer.println("<h3>The count is: " + counter + "</h3>" );
    }


}
