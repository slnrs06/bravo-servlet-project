import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ExampleServlet", urlPatterns = "/example-servlet")
public class ExampleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//        1. get the PrintWriter
        PrintWriter pw = response.getWriter();
//        2. generate the HTML content
        pw.println("<h1>Example Servlet</h1>");
        pw.println("<p>Created by Bravo</p>");
        pw.println("Time on the server is: " + new java.util.Date());

        pw.println("<a href=\"/hello\">Go to Hello Servlet</a>");
    }
}
