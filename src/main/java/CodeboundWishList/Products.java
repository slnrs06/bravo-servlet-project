package CodeboundWishList;

import java.util.List;

// DAO interface
public interface Products {
    List<Product> all(); // gets all of the products/items
    void insert(Product product); // adds the new product to the "db"
}
