package CodeboundWishList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowProductsServlet", urlPatterns = "/products")
public class ShowProductsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        Use the dao factory to get the dao object
        Products productsDao = ProductDaoFactory.getDao();

//        Use a method on the dao to get all the products
        List<Product> products = productsDao.all();

//        Pass the data to the jsp
//        two methods - .setAttribute(), .getRequestDispatcher
        request.setAttribute("listOfProducts", products);
//        request.getRequestDispatcher("file/path/to/specific/jsp-file").forward(request, response);

        request.getRequestDispatcher("/product-app/show-my-products.jsp").forward(request, response);
    }
}

