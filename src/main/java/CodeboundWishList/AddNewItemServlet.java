package CodeboundWishList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddNewItemServlet", urlPatterns = "/products/add-item")
public class AddNewItemServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //        Use the dao factory to get the dao object
        Products productsDao = ProductDaoFactory.getDao();

//        create a new product based on the submitted data
        String newName = request.getParameter("name");
        String newCategory = request.getParameter("category");
        double newPrice = Double.parseDouble(request.getParameter("price"));

        Product product = new Product(newName, newCategory, newPrice);

        // insert product to our "db"
        productsDao.insert(product);

        response.sendRedirect("/products");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/product-app/add-to-list.jsp").forward(request, response);
    }


}
