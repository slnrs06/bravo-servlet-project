package CodeboundWishList;

// class provides access to our DAO
public class ProductDaoFactory {
    private static Products productsDao;

    public static Products getDao() {
        if (productsDao == null) {
            productsDao = new ListProducts();
        }

        return productsDao;
    }
}

