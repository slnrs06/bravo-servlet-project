package CodeboundWishList;

import java.util.ArrayList;
import java.util.List;

// DAO Implementation
public class ListProducts implements Products {
    //    field
    private List<Product> products = new ArrayList<>();

    public ListProducts() {
        insert(new Product("Playstation 5", "Electronics", 499.99));
        insert(new Product("Impact Driver", "Tool", 199.97));
        insert(new Product("Nintendo Switch", "Electronics", 299.55));
    }




    @Override
    public void insert(Product product) {
        this.products.add(product);
    }

    @Override
    public List<Product> all() {
        return this.products;
    }
}

