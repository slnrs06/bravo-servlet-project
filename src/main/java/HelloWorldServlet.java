import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloWorldServlet", urlPatterns = "/hello-world")
public class HelloWorldServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // set to get a parameter
        String name = request.getParameter("name");

        //  set the PrintWriter
        PrintWriter writer = response.getWriter();

        if (name == null) {
            writer.println("<h1>Hello World!</h1>");
        }else {
            writer.println("<h1>Hello, " + name + "!</h1>");
        }

//        writer.println("<h1>Hello World!</h1>");

    }
}
//import javax.servlet.ServletException;
//        import javax.servlet.annotation.WebServlet;
//        import javax.servlet.http.HttpServletRequest;
//        import javax.servlet.http.HttpServletResponse;
//        import java.io.IOException;
//        import java.io.PrintWriter;
//
//@WebServlet(name = "HelloWorld", urlPatterns = "/hello-world")
//public class HelloWorld {
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//    }
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//
//        PrintWriter pw = response.getWriter();
////        2. generate the HTML content
//        pw.println("<h1>Hello from Ping Servlet</h1>");
//        pw.println("<p>Created by Bravo</p>");
//        pw.println("Time on the server is: " + new java.util.Date());
//
//        pw.println("<a href=\"/hello\">Go to PongServlet</a>");
//    }
//
//
//
//}
