<%--
  Created by IntelliJ IDEA.
  User: student01
  Date: 7/14/20
  Time: 10:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="bob" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student07
  Date: 7/14/20
  Time: 10:11 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Wish List</title>
</head>
<body>

<h1>Welcome to my WishList</h1>

<h3>Buy me these things por favor!</h3>

<%--    JSTL forEach directive--%>
<bob:forEach items="${listOfProducts}" var="product">

    <h3>${product.name}</h3>
    <p>${product.category}</p>
    <p>$ ${product.price}</p>

</bob:forEach>

<a href="/products/add-item">Add to my WishList</a>

</body>
</html>

