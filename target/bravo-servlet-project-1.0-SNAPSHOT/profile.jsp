<%--
  Created by IntelliJ IDEA.
  User: student01
  Date: 7/13/20
  Time: 9:24 AM
  To change this template use File | Settings | File Templates.
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
    <%@include file="WEB-INF/partials/bootstrap.jsp"%>
</head>
<body>
<%--Navbar included--%>
<%@include file="WEB-INF/partials/navbar.jsp"%>
<%-- Header --%>
<div class="jumbotron text-center">
    <h1>Welcome!</h1>
    <h3>A Website by a Codebound Instructor</h3>
</div>

<h1>You successfully logged in!</h1>

<div class="container">

    <div class="row">

        <div class="card col-3" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Counter Servlet</h5>
                <p class="card-text">Created a counter servlet that increases whenever visited.</p>
                <a href="/count" class="btn btn-primary">Go to project</a>
            </div>
        </div>

        <div class="card col-3" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Ping Servlet</h5>
                <p class="card-text">Created a servlet that's link to another servlet</p>
                <a href="/ping" class="btn btn-primary">Go to project</a>
            </div>
        </div>

        <div class="card col-3" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">WishList Project</h5>
                <p class="card-text">View my WishList Project</p>
                <a href="/products" class="btn btn-primary">Go to project</a>
            </div>
        </div>

    </div>

</div>



<%-- Footer Included--%>
<%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>

<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
<%--    <title>profile</title>--%>
<%--    <meta name="viewport" content="width=device-width, initial-scale=1">--%>
<%--    <style>--%>
<%--        body {--%>
<%--            margin: 0;--%>
<%--            font-family: Arial, Helvetica, sans-serif;--%>
<%--        }--%>
<%--        .topnav {--%>
<%--            overflow: hidden;--%>
<%--            background-color: #333;--%>
<%--        }--%>
<%--        .topnav a {--%>
<%--            float: left;--%>
<%--            color: #f2f2f2;--%>
<%--            text-align: center;--%>
<%--            padding: 14px 16px;--%>
<%--            text-decoration: none;--%>
<%--            font-size: 17px;--%>
<%--        }--%>
<%--        .topnav a:hover {--%>
<%--            background-color: #ddd;--%>
<%--            color: black;--%>
<%--        }--%>
<%--        .topnav a.active {--%>
<%--            background-color: #4CAF50;--%>
<%--            color: white;--%>
<%--        }--%>
<%--    </style>--%>
<%--</head>--%>
<%--<body>--%>
<%--<div class="topnav">--%>
<%--    <a class="active" href="/">Home</a>--%>
<%--    <a href="#news">News</a>--%>
<%--    <a href="#contact">Contact</a>--%>
<%--    <a href="#about">About</a>--%>
<%--</div>--%>
<%--</body>--%>
<%--</html>--%>
