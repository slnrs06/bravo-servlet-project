<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student01
  Date: 7/13/20
  Time: 9:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login form</title>
</head>
<body>

<h1>Please Login!</h1>
<form action="login.jsppost" method="post">
    <label>Username</label>
    <input type="text" id="username" placeholder="Username...">
    <label>Password</label>
    <input type="text" id="password" placeholder="Password...">
    <br>
    <input type="submit" value="Log in">
</form>
<c:choose>
    <c:when test="${param.username == 'admin' && param.password == 'password'}">
        <%
            response.sendRedirect("/profile.jsp");
        %>
    </c:when>
</c:choose>


<%--<%--%>
<%--    String user = request.getParameter("username");--%>
<%--    if (user == "admin"){--%>
<%--        response.sendRedirect("/WEB-INF/profile.jsp");--%>
<%--    } else {--%>
<%--        response.sendRedirect("/WEB-INF/login.jsp");--%>
<%--    }--%>
<%--%>--%>



</body>
</html>
